# Privy Intern C3 Delima Danarini

Project C3 PrivyID Intern - Berandal Studio Project Description

This project is a part of my internship project for PrivyID, containing design of Berandal Studio's website. Berandal Studio was a game development team that I made in Studi Independent Kampus Merdeka held by Indonesian Education, Culture & Technology Ministry in Agate International. One of the product I use in this project is a part of Berandal Studio.

1. Boilerplate
2. Database -> Supabase - https://qybwzteydngjxaqmaolk.supabase.co
3. Manajemen Dashboard -> https://deedan99.retool.com/apps/e7dc3e6c-4e99-11ed-bcf8-cfd811aca988/Project%20C3%20Delima%20Danarini
4. Endpoint Frontend
5. Frontend
6. Automation
7. Kanban, User Story -> https://trusting-dracopelta-4b9.notion.site/Berandal-Studio-Page-8f151edf7ae544c182921b0029959c0d
8. Design -> Figma - https://www.figma.com/file/ZmKN8JYOxnYqIH0QO0FnPD/Project-C3-Delima-Danarini?node-id=0%3A1
9. API Contract, ERD, Flow Diagram -> Whimsical - https://whimsical.com/c3-project-privyid-delima-danarini-berandal-studio-api-contract-VTjxE9LxFoX7XN3NgM8i5C 
10. Deployment
11. Project Description -> Markdown
12. Endpoint Retool

